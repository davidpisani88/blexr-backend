<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use App\User;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewUserRegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $email;
    protected $password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $user = User::where('email', $this->email)->first();

        return $this->view('emails.registerEmployee')->with([
            'user' => $user,
            'password' => $this->password
        ])
            ->subject("Blexr - Welcome to Blexr")
            ->from('davidpisani88@gmail.com', 'Blexr HR');
    }
}

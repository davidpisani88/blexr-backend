<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\UserRequests;
use App\Locations;
use Illuminate\Support\Facades\Mail;
use App\Mail\RequestStatusNotify;

class EmployeeRequestController extends Controller
{
    public function index() {

        $roles = \Auth::user()->roles->pluck('role_id')->toArray();

        //check if user is just a normal employee or employee admin
        if(!in_array(1, $roles)){
            $userRequests = UserRequests::where('id', \Auth::user()->id)->get();
        }else{
            $userRequests = UserRequests::all();
        }

        return view('pages.employeeRequest', compact('userRequests'));
    }
    
    public function apply() {

        $locations = Locations::all();

        return view('pages.employeeRequestApply', compact('locations'));
    }

    public function store(Request $request) {


        //validate form data
        $request->validate([
            'date' => 'required|date',
            'requestType' => 'required',
            'hoursCompleted' => 'sometimes|required',
            'location' => 'sometimes|required',
        ]);

        $date = $request->input('date');
        $type = $request->input('requestType');
        $hours = $request->input('hoursCompleted');
        $hiddenExtra = $request->input('hiddenExtra');
        $location = $request->input('location');

        $ud = Carbon::parse($date, 'Europe/Malta');
        $td = Carbon::now('Europe/Malta');

        $todayDateFormat = Carbon::now('Europe/Malta')->format('Y-m-d');
        $userDateFormat = Carbon::parse($date, 'Europe/Malta')->format('Y-m-d');

        $fourPm = Carbon::today('Europe/Malta')->subHours(8);


        $today = $td->format('Y-m-d');

        $pastDate = Carbon::parse($today.' 07:59:00');

        $message = false;

        // here i make the assumption that for remote and sick we can make requests in the past.
        if($type == 'remote' && $userDateFormat >= $todayDateFormat && $td->diffInHours($fourPm, false) < 0){
            $message = 'A remote request has to be made by 4PM of the previous day';
        }elseif($type == 'sick' && $td->format('Y-m-d H:i:s') > $pastDate && $ud->format('Y-m-d') >= $td->format('Y-m-d')){
            $message = 'Sick day request has to be made by 8am of that same work day';
        }

        $user = \Auth::user();

        if(!isset($hours)){
            $hours = 0;
        }

        if($message){
            return redirect('/employee/request/apply')->with('dateError', $message)->with('date', $date)->with('type', $type)->with('hiddenExtra', $hiddenExtra)->with('location', $location)->with('hours', $hours);
        }

        $insert = UserRequests::create([
            'user_id' => $user->id,
            'type' => $type,
            'location' => (int)$location,
            'date' => $ud->format('Y-m-d'),
            'hours' => (float)$hours,
        ]);

        return redirect('/employee/request')->with('flash', 'Request submitted');
    }

    public function statusUpdate(Request $request) {

        $id = $request->input('id');
        $status = $request->input('status');

        $ur = UserRequests::where('id', $id)->first();

        $ur->status = $status;
        $save = $ur->save();
        
        if($save){
            Mail::to($ur->user->email)
                ->send(new RequestStatusNotify($ur));

            return json_encode(true);
        }else{
            return json_encode(false);
        }
    }

    public function filter(Request $request) {

        $date = $request->input('date');
        $type = $request->input('type');
        $status = $request->input('status');

        $userRequests = UserRequests::query();

        if($type != 'reset') {
            if ($date) {

                $date = Carbon::parse($request->input('date'), 'Europe/Malta');
                $userRequests->whereDate('date', '=', $date);
            }

            if ($type) {
                $userRequests->where('type', $type);
            }

            if ($status) {
                $userRequests->where('status', $status);
            }
        }

        $result = $userRequests->get();


        $html ='';
        if($result->count() > 0){
            foreach($result as $res){

                $location = '-';

                if(isset($res->remote_location->title)){
                    $location = $res->remote_location->title;
                }

                $html .= '
                <tr data-id="'.$res->id.'">
                    <td>'.$res->user->name.'</td>
                    <td>'.\Carbon\Carbon::parse($res->date)->format('jS M Y').'</td>
                    <td scope="row">'.$res->type.'</td>
                    <td scope="row">'.$location.'</td>
                    <td>'.$res->hours.'</td>
                    <td class="reqStatus">'.$res->status.'</td>
                    <td><button data-status="approved" type="button" class="btn btn-success status-btn">Approve</button></td>
                    <td><button data-status="rejected" type="button" class="btn btn-danger status-btn">Reject</button></td>
                </tr>';
            }
        }else{
            $html .= '<p class="noResults">Sorry :( no results found.</p>';
        }


        return json_encode($html);
    }
    
}

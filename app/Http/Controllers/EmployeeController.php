<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class EmployeeController extends Controller
{
    public function __construct()
    {
        // only admin users can access this class
        $this->middleware('user.role:1');
    }

    public function employeeList() {

        $users = User::all();
        return view('pages.usersList')->with(['users' => $users]);
    }

    public function getEmployee($id) {

        $user = User::where('id', $id)->first();
        return view('pages.employeeProfile')->with(['user' => $user]);
    }

    public function licenseUpdate(Request $request, $id) {

        $user = User::where('id', $id)->first();

        $email = $request->input('email_access');
        $trello = $request->input('trello');
        $git = $request->input('git');
        $microsoft = $request->input('microsoft');


        //check if checkboxes are set, if they are set value = 1 else value = 0
        if(isset($email)){
            $user->email_access = 1;
        }else{
            $user->email_access = 0;
        }
        if(isset($trello)){
            $user->trello = 1;
        }else{
            $user->trello = 0;
        }
        if(isset($git)){
            $user->git = 1;
        }else{
            $user->git = 0;
        }
        if(isset($microsoft)){
            $user->microsoft = 1;
        }else{
            $user->microsoft = 0;
        }

        $user->save();

        return redirect('/employee/'.$id);
    }
}

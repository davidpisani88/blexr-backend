<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserRoles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserRegistrationEmail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterEmployeeController extends Controller
{

    public function store(Request $request) {

        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ]);

        if($validatedData) {
            $password = str_random(10);

            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($password),
            ]);

            UserRoles::create([
                'user_id' => $user->id,
                'role_id' => 2, //employee
            ]);

            Mail::to($user->email)
                ->send(new NewUserRegistrationEmail($request->input('email'), $password));

            return redirect('/employee/list')->with('flash', 'Employee added');
        }
    }

}

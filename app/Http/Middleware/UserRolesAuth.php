<?php

namespace App\Http\Middleware;

use Closure;

class UserRolesAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        if(in_array($role, \Auth::user()->roles->pluck('role_id')->toArray())){
            return $next($request);

        }else{
            abort(404);
        }
    }

}

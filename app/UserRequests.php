<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequests extends Model
{
    protected $table = 'user_requests';

    public $timestamps = false;

    protected $fillable = [
        'user_id',  'type',  'date',  'hours',  'location'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function remote_location() {
        return $this->belongsTo('App\Locations', 'location');
    }
}

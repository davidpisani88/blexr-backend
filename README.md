SQL script:
app/blexr-sql-script.sql

Login:
email: blexr-user@blexr.com
password: blexr123

Assumptions

1. Sick leave and remote work can be lodged for dates in the past.
2. For below part I assume that the day ends at 24 hours so for a) the time would be 4PM of the previous day.
    a). A request has to be made at least 8hours before the end of the previous day


env:

APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:sT0pTVjD6mSyLS+R17UD2sGmNoJFIBiJfcHDPwXXykw=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=blexr
DB_USERNAME=
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
SESSION_DRIVER=file
SESSION_LIFETIME=120
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=addyour@own.com
MAIL_PASSWORD=
MAIL_ENCRYPTION=tls

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

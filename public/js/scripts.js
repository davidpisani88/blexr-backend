var csrf_token = $('meta[name="csrf-token"]').attr('content');
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": csrf_token
    }
});

$(document).ready(function() {

    if($( ".datepicker" ).length){
        $( ".datepicker" ).datepicker();
    }

    $('.datepicker').on('click', function(e) {
        e.preventDefault();
        $(this).attr("autocomplete", "off");
    });
    
    $('#requestType').change(function (e) {
        var type = $(this).find(':selected').val();
        var extra = $(this).attr('data-extra');

        $('#hiddenExtra').val(type);

        if(type == 'remote'){
            $('.hrsComp').show();
            $('.hrsComp input, .hrsComp select').prop('disabled', false);
        }else{
            $('.hrsComp').hide();
            $('.hrsComp input, .hrsComp select').prop('disabled', true);
        }
    });

    $('body').on('click', '.status-btn', function(e) {
        e.preventDefault();

        var elem = $(this);
        var status = elem.attr('data-status');
        var id = elem.parent('td').parent('tr').attr('data-id');

        elem.html('submitting...');
        elem.prop('disabled', true);
        
        $.ajax({
            url: '/employee/request/status-update',
            dataType: 'json',
            type: 'POST',
            data: 'id='+id+'&status='+status,
            success: function(data) {

                elem.parent('td').siblings('.reqStatus').html(status);

                if(status == 'rejected'){
                    elem.html('Reject');
                }else{
                    elem.html('Approve');
                }

                elem.prop('disabled', false);
            },
            error: function(data) {
            }
        });
    });


    $('.filterReset').click(function(e){
        var formData = 'type=reset';
        requestFilter(formData)
    });
    

    $('#requestFilterForm').submit(function(e){
        e.preventDefault();

        var formData = $(this).serialize();
        requestFilter(formData)
    });

    function requestFilter(formData){
        $.ajax({
            url: '/employee/request/filter',
            dataType: 'json',
            type: 'POST',
            data: formData,
            success: function(data) {
                $('#requestsTableBody').html(data);
            },
            error: function(data) {
            }
        });
    }

});
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Blexr | Request status change</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<p>Hello {{$request->user->name}},</p>
<p>Your request for date <strong>{{\Carbon\Carbon::parse($request->date)->format('D jS M Y')}}</strong> has changed status to <strong>{{ucfirst($request->status)}}</strong></p>
</body>

</html>
@include('partials.header')
<body>
    <div id="app">

        @include('partials.mainNav')

        <main class="py-4">

            <div class="container">
                <div class="row justify-content-center">
                    @if(Auth::user())
                        <div class="col-md-3">
                            <ul id="sideMenu">
                                @if(in_array(1, Auth::user()->roles->pluck('role_id')->toArray()))
                                    <li><a href="/employee/list">Employees</a></li>
                                    <li><a href="/employee/create">Add employee</a></li>
                                    <li><a href="/employee/request">Requests</a></li>
                                @endif
                                @if(in_array(2, Auth::user()->roles->pluck('role_id')->toArray()))
                                    <li><a href="/employee/request">Requests</a></li>
                                @endif
                            </ul>
                        </div>
                    @endif
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">@yield('page-title')</div>

                            <div class="card-body">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    </div>
</body>
</html>

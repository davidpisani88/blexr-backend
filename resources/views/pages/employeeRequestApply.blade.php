@extends('layouts.app')

@section('page-title')
    Request | Apply
@stop

@section('content')


    <?php

        $check = Session::get('hiddenExtra');
        $hours = false;

        if ($errors->count() > 0 && old('requestType') == 'remote'){
            $hours = true;
        }elseif(isset($check) && $check == 'remote'){
            $hours = true;
        }elseif(old('requestType') == 'remote'){
            $hours = true;
        }



    ?>

    <form id="requestForm" method="POST" action="/employee/request/store">
        @csrf
        <div class="col-md-6">
            <div class="form-group">
                <label for="requestType">Leave type:</label>
                <select class="form-control" id="requestType" name="requestType">
                    <option value="">Select leave type</option>
                    <option data-extra="remote" @if($check && Session::get('type') == 'remote') selected @elseif(old('requestType') == 'remote') selected @endif value="remote">Remote</option>
                    <option data-extra="" @if($check && Session::get('type') == 'sick') selected @elseif(old('requestType') == 'sick') selected @endif value="sick">Sick leave</option>
                </select>

                @if ($errors->has('requestType'))
                    <span class="invalid-feedback" role="alert" style="display: block;">
                     <strong>{{($errors->first('requestType'))}}</strong>
                    </span>
                @endif
            </div>




            <input type="hidden" @if($check) value="{{$check}}" @else  value="{{old('hiddenExtra')}}" @endif name="hiddenExtra" id="hiddenExtra">

            <div class="form-group">
                <label for="date">Date:</label>
                <input @if(($check)) value="{{Session::get('date')}}" @else  value="{{old('date')}}" @endif autocomplete="off" type="text" class="form-control datepicker" id="datepicker" name="date">

                @if ($errors->has('date'))
                    <span class="invalid-feedback" role="alert" style="display: block;">
                     <strong>{{($errors->first('date'))}}</strong>
                    </span>
                @endif

                @if ($message = Session::get('dateError'))
                    <span class="invalid-feedback" role="alert" style="display: block;">
                     <strong>{{$message}}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group hrsComp" @if(!$hours) style="display: none;" @endif>
                <label for="location">Location:</label>
                <select @if(!$hours) disabled @endif class="form-control" id="location" name="location">
                    <option value="">Select location</option>
                    @foreach($locations as $loc)
                        <option @if($check && (Session::get('location') == $loc->name)) selected  @elseif(old('location') == $loc->id) selected @endif data-extra="{{$loc->extra}}" value="{{$loc->id}}">{{$loc->title}}</option>
                    @endforeach
                </select>

                @if ($errors->has('location'))
                    <span class="invalid-feedback" role="alert" style="display: block;">
                     <strong>{{($errors->first('location'))}}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group hrsComp"  @if(!$hours) style="display: none;" @endif>
                <label for="requestType">Hours completed:</label>
                <input @if(!$hours) disabled @endif @if($check) value="{{Session::get('hours')}}" @else value="{{old('hoursCompleted')}}" @endif type="number" class="form-control"  id="hoursCompleted" name="hoursCompleted">

                @if ($errors->has('hoursCompleted'))
                    <span class="invalid-feedback" role="alert" style="display: block;">
                     <strong>{{($errors->first('hoursCompleted'))}}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

        </div>

    </form>
@stop

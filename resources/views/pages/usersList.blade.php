@extends('layouts.app')

@section('page-title')
    Employee list
@stop

@section('content')

    @if ($message = Session::get('flash'))
        <div class="alert alert-success" role="alert">
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif

    <ul>
    @foreach($users as $user)
        <li><a href="/employee/{{$user->id}}">{{$user->name}}</a></li>
    @endforeach
    </ul>
@stop

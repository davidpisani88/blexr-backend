@extends('layouts.app')

@section('page-title')
    {{$user->name}}
@stop

@section('content')

    <form method="POST" action="/employee/{{$user->id}}/licenses/update" aria-label="{{ __('Register') }}">
        @csrf
        <div class="form-group form-check">
            <input value="1" type="checkbox" class="form-check-input" id="email_access" name="email_access" @if($user->email_access == 1) checked @endif>
            <label class="form-check-label" for="email_access">Email access granted</label>
        </div>

        <div class="form-group form-check">
            <input value="1" type="checkbox" class="form-check-input" id="trello" name="trello" @if($user->trello == 1) checked @endif>
            <label class="form-check-label" for="trello">Trello access granted</label>
        </div>

        <div class="form-group form-check">
            <input value="1" type="checkbox" class="form-check-input" id="git" name="git" @if($user->git == 1) checked @endif>
            <label class="form-check-label" for="git">Git repository granted</label>
        </div>

        <div class="form-group form-check">
            <input value="1" name="microsoft" type="checkbox" class="form-check-input" id="microsoft" @if($user->microsoft == 1) checked @endif>
            <label class="form-check-label" for="microsoft">Microsoft Office licence</label>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@stop

@extends('layouts.app')

@section('page-title')
    Requests
@stop

@section('content')

    @if ($message = Session::get('flash'))
        <div class="alert alert-success" role="alert">
            <p class="mb-0">{{ $message }}</p>
        </div>
    @endif
    <form id="requestFilterForm" action="">

        <div class="form-row">
            <div class="col-md-4">
                <a class="btn btn-primary requestApplyBtn" href="/employee/request/apply" role="button">+ Apply</a>
            </div>

        </div>
        <div class="form-row" id="filterHolder">
            <div class="col-md-3" >
                <input autocomplete="off" type="text" class="form-control datepicker"  id="datepicker" name="date" value="{{old('date')}}" placeholder="Date">
            </div>
            <div class="col-md-4">
                <select class="form-control" name="type" id="">
                    <option value="">Select leave type</option>
                    <option value="remote">Remote</option>
                    <option value="sick">Sick</option>
                </select>
            </div>

            <div class="col-md-3">
                <select class="form-control" name="status" id="">
                    <option value="">Select status</option>
                    <option value="unapproved">Unapproved</option>
                    <option value="approved">Approved</option>
                    <option value="rejected">Rejected</option>
                </select>
            </div>

            <div class="col-md-1 text-left">
                <button type="submit" class="btn btn-info filterBtn">Filter</button>
            </div>
            <div class="col-md-1 text-left">
                <div class="filterReset filterBtn">X</div>
            </div>

        </div>
    </form>
    <div class="row">
        <div class="col-md-12">
            <table id="employeeRequestsTable" class="table">
                <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Date</th>
                    <th scope="col">Type</th>
                    <th scope="col">Location</th>
                    <th scope="col">Hours</th>
                    <th scope="col">Status</th>
                    <th scope="col">Approve</th>
                    <th scope="col">Reject</th>
                </tr>
                </thead>
                <tbody id="requestsTableBody">
                @foreach($userRequests as $req)
                    <tr data-id="{{$req->id}}">
                        <td>{{$req->user->name}}</td>
                        <td>{{\Carbon\Carbon::parse($req->date)->format('jS M Y')}}</td>
                        <td scope="row">{{$req->type}}</td>
                        <td scope="row">@if(isset($req->remote_location->title)){{$req->remote_location->title}}@else - @endif</td>
                        <td>{{$req->hours}}</td>
                        <td class="reqStatus">{{$req->status}}</td>
                        <td><button data-status="approved" type="button" class="btn btn-success status-btn">Approve</button></td>
                        <td><button data-status="rejected" type="button" class="btn btn-danger status-btn">Reject</button></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

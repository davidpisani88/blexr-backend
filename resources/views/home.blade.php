@extends('layouts.app')

@section('page-title')
    Dashboard
@stop

@section('content')
    <p>Welcome to Blexr's new Employment Management System. Please use the left hand navigation to explore.</p>

    <p>More exciting features are on their way.</p>
@endsection

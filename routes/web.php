<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');


Route::get('/employee/create', function () {
    return view('auth.registerEmployee');
});

Route::post('/employee/store', 'Auth\RegisterEmployeeController@store');
Route::post('/employee/request/status-update', 'EmployeeRequestController@statusUpdate');

Route::get('/employee/list', 'EmployeeController@employeeList');
Route::get('/employee/request', 'EmployeeRequestController@index');
Route::post('/employee/request/filter', 'EmployeeRequestController@filter');
Route::get('/employee/request/apply', 'EmployeeRequestController@apply');
Route::post('/employee/request/store', 'EmployeeRequestController@store');
Route::post('/employee/{id}/licenses/update', 'EmployeeController@licenseUpdate');
Route::get('/employee/{id}', 'EmployeeController@getEmployee');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
